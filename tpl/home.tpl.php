<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Home</title>


<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">


	
</head>
<body>

	<h1 class="text-center">Blog</h1>
	<div class="container">
	<?php if ($result->num_rows > 0): ?>
		<?php while($row = $result->fetch_assoc()): ?>
		<a href="?act=article&id=<?php echo  $row['id'] ?>" class="row">
			<h3 class="col-6"><?php echo $row["title"]; ?></h3>
			<h6 class="col-6 text-right"><?php echo date("Y-m-d, H:m:s", $row["tite"]) ?></h6>
		</a>
		<div class="row col-12">
			<div class="col-3">
				<img src="<?php echo $row["prev_img"] ?>" alt="">
			</div>
			<div class="col-9">
				<?php echo $row["prev"]; ?>
			</div>	
		</div>
		<?php endwhile ?>
	<?php else: ?>
	<h2>No records Yet</h2>
	<?php endif ?>
	</div>
</body>
</html>