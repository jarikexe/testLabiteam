<?php
require_once("conf.php");
require_once("lib/func.php");

$act = isset($_GET['act']) ? $_GET['act'] : 'list';
switch ($act) {
	case "main":
		$conn = new mysqli(SERVER, USER_NAME, PASS, DB);
		if ($conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		} 
		$sql = "SELECT id, date, title, prev, prev_img, text FROM records";
		$result = $conn->query($sql);
		$conn->close();
		require_once("tpl/home.tpl.php");
	break;


	case "article":
		$id = $_GET["id"];

		$conn = new mysqli(SERVER, USER_NAME, PASS, DB);
		if ($conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		} 
		$sql = "SELECT id, date, title, prev, prev_img, text FROM records WHERE id = '$id'";
		$result = $conn->query($sql);

		$sql = "SELECT id, title FROM records LIMIT 20";
		$result2 = $conn->query($sql);

		$conn->close();
		require_once("tpl/article.tpl.php");
	break;
	case "admin":
		require_once("tpl/admin.tpl.php");
		
	break;

	case "add":

		$conn = new mysqli(SERVER, USER_NAME, PASS, DB);
		if ($conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		} 

		$stmt = $conn->prepare("INSERT INTO records (title, prev, text, prev_img, date) VALUES (?, ?, ?, ?, ?)");

		$stmt->bind_param("ssssi", $title, $prev, $full,$uploadfile,$time);

		$time = time();
		$title = filt($_POST["title"]);
		$prev = $_POST["prev"];
		$full = $_POST["full"];
		
		
		$uploaddir = './prev/';
		$uploadfile = $uploaddir . date("Y-d-m-G-i-s-u") . basename($_FILES['img']['name']);
		if($_FILES['img']['type'] == "image/png" || $_FILES['img']['type'] == "image/gif" || $_FILES['img']['type'] == "image/jpeg"){
			$img = imagescale(imagecreatefromstring(file_get_contents($_FILES['img']['tmp_name'])) , 100);
			if(imagejpeg($img, $uploadfile, 75) && $stmt->execute()) header("Location: .");
			else header("Location: ?act=admin");
		}else{
			$conn->close();
			exit("The file is not an image");
		}
		$conn->close();


	break;
	default:
		header("Location: ?act=main");
	break;
}