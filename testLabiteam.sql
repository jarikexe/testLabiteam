-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Ноя 21 2017 г., 04:27
-- Версия сервера: 5.5.50
-- Версия PHP: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `testLabiteam`
--

-- --------------------------------------------------------

--
-- Структура таблицы `records`
--

CREATE TABLE IF NOT EXISTS `records` (
  `id` int(11) NOT NULL,
  `date` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `prev` tinytext NOT NULL,
  `prev_img` varchar(255) NOT NULL,
  `text` mediumtext NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `records`
--

INSERT INTO `records` (`id`, `date`, `title`, `prev`, `prev_img`, `text`) VALUES
(24, 1511226596, 'Python for Everybody\\: Exploring Data in Python 3', 'Python for Everybody is designed to introduce students to programming and software development through the lens of exploring data. You can think of the Python programming language as your tool to solve data problems that are beyond the capability of a spr', './prev/2017-21-11-4-09-56-00000051lpFL7BFZL.jpg', 'Python for Everybody is designed to introduce students to programming and software development through the lens of exploring data. You can think of the Python programming language as your tool to solve data problems that are beyond the capability of a spreadsheet.\r\n\r\nProduct details\r\nFile Size: 3880 KB\r\nPrint Length: 242 pages\r\nSimultaneous Device Usage: Unlimited\r\nPublication Date: July 10, 2016\r\nSold by: Amazon Digital Services LLC\r\nLanguage: English\r\nASIN: B01IA5VIFM\r\nText-to-Speech: Enabled  \r\nX-Ray:\r\nNot Enabled  \r\nWord Wise: Not Enabled\r\nLending: Not Enabled\r\nEnhanced Typesetting: Not Enabled  \r\nAmazon Best Sellers Rank: #5,213 Paid in Kindle Store (See Top 100 Paid in Kindle Store)\r\n#2 in Kindle Store > Kindle eBooks > Computers & Technology > Programming > Languages & Tools > Python\r\n#3 in Books > Computers & Technology > Programming > Languages & Tools > Python\r\n#4 in Books > Computers & Technology > Programming > Web Programming\r\nWould you like to tell us about a lower price?'),
(25, 1511226765, 'Python for Kids', 'Python is a powerful, expressive programming language that’s easy to learn and fun to use! But books about learning to program in Python can be kind of dull, gray, and boring, and that’s no fun for anyone.', './prev/2017-21-11-4-12-45-000000510p5CCH08L._SX376_BO1,204,203,200_.jpg', 'Python is a powerful, expressive programming language that’s easy to learn and fun to use! But books about learning to program in Python can be kind of dull, gray, and boring, and that’s no fun for anyone.\r\n\r\nPython for Kids brings Python to life and brings you (and your parents) into the world of programming. The ever-patient Jason R. Briggs will guide you through the basics as you experiment with unique (and often hilarious) example programs that feature ravenous monsters, secret agents, thieving ravens, and more. New terms are defined; code is colored, dissected, and explained; and quirky, full-color illustrations keep things on the lighter side.\r\n\r\nChapters end with programming puzzles designed to stretch your brain and strengthen your understanding. By the end of the book you’ll have programmed two complete games: a clone of the famous Pong and "Mr. Stick Man Races for the Exit"—a platform game with jumps, animation, and much more.\r\n\r\nAs you strike out on your programming adventure, you’ll learn how to:\r\n–Use fundamental data structures like lists, tuples, and maps\r\n–Organize and reuse your code with functions and modules\r\n–Use control structures like loops and conditional statements\r\n–Draw shapes and patterns with Python’s turtle module\r\n–Create games, animations, and other graphical wonders with tkinter\r\n\r\nWhy should serious adults have all the fun? Python for Kids is your ticket into the amazing world of computer programming.'),
(26, 1511226994, 'Say thanks', 'Crediting isn’t required', './prev/2017-21-11-4-16-34-000000emily-sea-198689.jpg', 'Crediting isn’t required, but is appreciated and allows photographers to gain exposure. Copy the text below or embed a credit badge\r\n\r\nPhoto by Emily Sea on Unsplash'),
(27, 1511227407, 'Raspberry Pi 3\\: Beginner to Pro', 'Learn all about the Raspberry Pi3 and what you can do with it. No previous technical skills required!\r\nNow with fully update URLs in the paperback version for easy reference The Raspberry Pi 3 is a powerful minicomputer that has gained popularity for its ', './prev/2017-21-11-4-23-27-00000041pEMpRsZwL._SX326_BO1,204,203,200_.jpg', 'Learn all about the Raspberry Pi3 and what you can do with it. No previous technical skills required!\r\nNow with fully update URLs in the paperback version for easy reference The Raspberry Pi 3 is a powerful minicomputer that has gained popularity for its versatility with hobbyists, DIYers, students, and digital engineers. More than just a tiny computer, the RPI is also a microcontroller that can light LEDs, spin motors, obtain sensor input, and much more. This book serves as a beginners guide to the RPI 3. Including how to acquire the device, we also explain how to set it up and get going building your own projects- no prior experience with electronics is necessary! Between offering project ideas and links to popular peripherals, this book will keep the RPI owner busy learning and exploring the world around them with their Raspberry Pi. For children and adults alike, the RPI is an amazing device that fosters creativity; this book is the starter guide for their journey. This book will save you hours upon hours of trying to figure this stuff out for yourself! This book includes: What is the Raspberry Pi 3 How to set up RPI 3 How to use Raspbian GPIO Pins Project Ideas Accessories for the Pi Advanced Circuits with the RPI 3 Raspberry Pi Tips and Tricks Take action now and buy this book to start your journey towards Raspberry Pi 3 mastery! Free next day delivery for Amazon Prime Customers. Tags: Raspberry Pi, Raspberry Pi 3, Raspberry Pi 3 Book, Raspberry Pi 3 Guide, Raspberry Pi 3 Kindle, Raspberry Pi 3 2016, Raspberry Pi 3 Projects, Raspberry Pi 3 User Guide, Raspberry Pi 3 Programming, Raspberry Pi 3 Python, Raspberry Pi 3 Beginners');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `records`
--
ALTER TABLE `records`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `records`
--
ALTER TABLE `records`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
